"""
Setup configuration
"""
import setuptools
import pyuntappd

VERSION = pyuntappd.__version__

with open("README.md", "r") as fh:
    LONG = fh.read()
setuptools.setup(
    name="pyuntappd",
    version=VERSION,
    author="Joakim Sorensen",
    author_email="joasoe@gmail.com",
    description="",
    long_description=LONG,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ludeeus/pyuntappd",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)